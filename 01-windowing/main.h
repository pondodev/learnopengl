#ifndef MAIN_H
#define MAIN_H

// this is to make vscode stop having a whinge, not needed for compilation
#ifndef __glad_h_
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#endif

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

#endif