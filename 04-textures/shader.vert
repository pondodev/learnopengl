#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;

out vec3 vertexColor;
out vec2 TexCoord;

uniform float time;

void main() {
    gl_Position = vec4(aPos.x + sin(time * 5.0) / 5.0, aPos.y + sin(time) / 5.0, aPos.z, 1.0);
    vertexColor = aColor;
    TexCoord = aTexCoord;
}